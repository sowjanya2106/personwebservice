package com.app.json;

import com.app.model.Person;

public class SinglePersonResponse {
	
		private boolean success;
		private Person person;
		
		public SinglePersonResponse(boolean success, Person person) {
			super();
			this.success = success;
			this.person = person;
		}

		public boolean isSuccess() {
			return success;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}

		public Person getPerson() {
			return person;
		}

		public void setPerson(Person person) {
			this.person = person;
		}
		

}
