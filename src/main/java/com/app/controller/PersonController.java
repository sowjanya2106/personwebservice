package com.app.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.dao.PersonRepository;
import com.app.json.MultiplePersonResponse;
import com.app.json.RestResponse;
import com.app.json.SinglePersonResponse;
import com.app.model.Address;
import com.app.model.Person;
import com.app.model.Phone;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class PersonController {

	@Autowired
	private PersonRepository personRepository;

	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	@ResponseBody
	public MultiplePersonResponse getAllPersons() {

		List<Person> allPersons = personRepository.getAllPersons();
		MultiplePersonResponse extResp = new MultiplePersonResponse(true,
				allPersons);

		return extResp;
	}

	@RequestMapping(value = "/person/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public SinglePersonResponse getPersonById(@PathVariable("id") String id) {
		Person person = personRepository.getPersonById(id);
		SinglePersonResponse extResp;
		if (person != null) {
			extResp = new SinglePersonResponse(true, person);
		} else {
			extResp = new SinglePersonResponse(true, person);
		}

		return extResp;
	}

	@RequestMapping(value = "/person", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public RestResponse addPerson(@RequestBody Person person) throws JsonParseException, JsonMappingException, IOException {
		RestResponse extResp = null;
		if (person.getName() != null && person.getName().length() > 0) {
			Person p = personRepository.addPerson(person);
			if(p != null) {
				extResp = new RestResponse(true, "Successfully added Person: "
					+ person.getName());
			}
			else {
				new RestResponse(false, "Failed to insert...");
			}
		} else {
			extResp = new RestResponse(false, "Failed to insert...");
		}

		return extResp;
	}

	@RequestMapping(value = "/person/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public RestResponse updatePersonById(@PathVariable("id") String id , @RequestBody Person person) 
			throws JsonParseException, JsonMappingException, IOException{
		RestResponse extResp;
		Person myPerson = personRepository.updatePerson(id, person);

		if (myPerson != null) {
			extResp = new RestResponse(true, "Successfully updated Person: "
					+ myPerson.toString());
		} else {
			extResp = new RestResponse(false, "Failed to update Person: " + id);
		}

		return extResp;
	}
	
	@RequestMapping(value = "/person/address/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public RestResponse updateAdressByPersonId(@PathVariable("id") String id , @RequestBody Address address) 
			throws JsonParseException, JsonMappingException, IOException{
		RestResponse extResp;
		Person myPerson = personRepository.updateAddress(id, address);

		if (myPerson != null) {
			extResp = new RestResponse(true, "Successfully updated Person: "
					+ myPerson.toString());
		} else {
			extResp = new RestResponse(false, "Failed to update Person: " + id);
		}

		return extResp;
	}
	
	@RequestMapping(value = "/person/phone/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public RestResponse updatePhoneByPersonId(@PathVariable("id") String id , @RequestBody Phone phone) 
			throws JsonParseException, JsonMappingException, IOException{
		RestResponse extResp;
		Person myPerson = personRepository.updatePhone(id, phone);

		if (myPerson != null) {
			extResp = new RestResponse(true, "Successfully updated Person: "
					+ myPerson.toString());
		} else {
			extResp = new RestResponse(false, "Failed to update Person: " + id);
		}

		return extResp;
	}



	@RequestMapping(value = "/person/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public RestResponse deleteUser(@PathVariable("id") String id) {
		personRepository.deletePerson(id);
		return new RestResponse(true, "Successfully deleted Person: " + id);
	}

}
