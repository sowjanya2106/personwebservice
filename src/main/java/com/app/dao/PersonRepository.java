package com.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.app.model.Address;
import com.app.model.Person;
import com.app.model.Phone;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;

@Repository
public class PersonRepository {

	public static final String COLLECTION_NAME = "persons";

	@Autowired
	@Qualifier("mongoTemplate")
	MongoTemplate mongoTemplate;

	public Person addPerson(Person person) {
		Person person2 = mongoTemplate.findOne(
				Query.query(Criteria.where("_id").is(person.getId())),
				Person.class, COLLECTION_NAME);
		if (person2 == null) {
			mongoTemplate.save(person, COLLECTION_NAME);
		} else {
			person = null;
		}

		return person;
	}

	public Person getPersonById(String id) {
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("_id", id);
		DBCursor cursor = mongoTemplate.getCollection(COLLECTION_NAME).find(
				whereQuery);
		return mongoTemplate.getConverter().read(Person.class, cursor.next());
		
	}

	public List<Person> getAllPersons() {
		DBCursor cursor = mongoTemplate.getCollection(COLLECTION_NAME).find();
		List<Person> list = new ArrayList<Person>();

		while (cursor.hasNext()) {
			DBObject obj = cursor.next();
			Person person = mongoTemplate.getConverter()
					.read(Person.class, obj);
			list.add(person);
		}
		return list;
		

	}

	public Person deletePerson(String id) {
		Person person = mongoTemplate.findOne(
				Query.query(Criteria.where("_id").is(id)), Person.class,
				COLLECTION_NAME);
		mongoTemplate.remove(person, COLLECTION_NAME);

		return person;
	}

	public Person updatePerson(String id, Person person) {

		if (id.equals(person.getId())) {
			mongoTemplate.save(person, COLLECTION_NAME);
			return person;
		}

		return null;

	}
	
	public Person updateAddress(String id,Address address){
		Person person = mongoTemplate.findOne(
				Query.query(Criteria.where("_id").is(id)), Person.class,
				COLLECTION_NAME);
		if(person != null){
			person.setAddress(address);
			mongoTemplate.save(person,COLLECTION_NAME);
			return person;
		}
		
		return null;
	}
	
	public Person updatePhone(String id,Phone phone){
		Person person = mongoTemplate.findOne(
				Query.query(Criteria.where("_id").is(id)), Person.class,
				COLLECTION_NAME);
		if(person != null){
			person.setPhone(phone);
			mongoTemplate.save(person,COLLECTION_NAME);
			return person;
		}
		
		return null;
	}

}
