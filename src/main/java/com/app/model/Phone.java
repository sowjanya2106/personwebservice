package com.app.model;

import org.springframework.data.mongodb.core.mapping.Document;


public class Phone {
	
	public String work;
	public String home;
	
	public Phone() {
	}
	
	public Phone(String work, String home) {
		super();
		this.work = work;
		this.home = home;
	}
	public String getWork() {
		return work;
	}
	public void setWork(String work) {
		this.work = work;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
}
