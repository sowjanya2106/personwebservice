package com.app.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class Person {
	
	@Id
	private String id;
	private String name;
	private String ssn;
	private String email;
	private Address address;
	private Phone phone;
	
	public Person() {
		
	}
	public Person(String id, String name, String ssn, String email,Address address,Phone phone) {
		super();
		this.id = id;
		this.name = name;
		this.ssn = ssn;
		this.email = email;
		this.address = address;
		this.phone = phone;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Phone getPhone() {
		return phone;
	}
	public void setPhone(Phone phone) {
		this.phone = phone;
	}
	public String toString() {
	    return "[" + getId()
	        + ", " + getName()
	        + ", " + getSsn()
	        + ", " + getEmail()
	        + "]";
	  }

}
