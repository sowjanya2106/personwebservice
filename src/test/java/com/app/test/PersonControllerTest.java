package com.app.test;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.net.UnknownHostException;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.app.controller.PersonController;
import com.app.dao.PersonRepository;
import com.app.model.Address;
import com.app.model.Person;
import com.app.model.Phone;
import com.mongodb.MongoException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application-config-test.xml")
@WebAppConfiguration
public class PersonControllerTest {
	
	@Mock
    Person person;
    
    @InjectMocks
    private PersonController personController;
    
    @Mock
    private PersonRepository personRepository;
    
    private MockMvc mockMvc;
   
    @Autowired
    MongoTemplate mongoTemplate;
   
   @Autowired
   private WebApplicationContext webApplicationContext;
   
   
  
    @Before
    public void setup() throws UnknownHostException, MongoException {
    	
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        personRepository = Mockito.mock(PersonRepository.class);
      //  personRepository = new PersonRepository(mongoTemplate);
       
    }
    Address address = new Address("test","test","CA","USA","12345");
    Phone phone = new Phone("12345667890","9012345678");
    
    Person p1 = new Person("20","testf2 testl2","222-22-2222","test2@gmail.com",address,phone);
    Person p2 = new Person("20","testf3 testl3","333-33-3333","test3@gmail.com",address,phone);
   
    @Test
    public void testSave() throws Exception {
    	
        mockMvc.perform(
                post("/person")
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(p1)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("success", is(true)));
    }
    
    @Test
    public void testGet() throws Exception {

        String userId = "20";
     //when(personRepository.getPersonById(userId)).thenReturn(new Person());
        mockMvc.perform(
                get("/person/" + userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("person.name").value(IsEqual.equalTo("testf2 testl2")))
                .andExpect(jsonPath("person.ssn").value(IsEqual.equalTo("222-22-2222"))) 
                .andExpect(jsonPath("person.id").value(IsEqual.equalTo(userId)))
                .andExpect(jsonPath("person.email").value(IsEqual.equalTo("test2@gmail.com")))
                .andExpect(jsonPath("success", is(true)));
    }
    
    @Test
    public void testUpdate() throws Exception {
        mockMvc.perform(
                put("/person/20")
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(p2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("success", is(true)));
    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(
                delete("/person/20"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("success", is(true)));
    }

}
